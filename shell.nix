{
  system ? "x86_64-linux",
  pkgs ? import <nixpkgs> { inherit system; },
}:

let
  packages = [
    (pkgs.python3.withPackages (
      ps: with ps; [
        requests
        packaging
      ]
    ))
    pkgs.zsh
    pkgs.gnumake
  ];

  LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath [
    pkgs.stdenv.cc.cc
  ];
in
pkgs.mkShell {
  buildInputs = packages;
  shellHook = ''
    export SHELL=${pkgs.zsh}
    export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}"
  '';
}

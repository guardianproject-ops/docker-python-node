import requests
import json
import sys
import re
from packaging import version
from typing import List, Dict, Any


def fetch_image_tags(image: str) -> List[str]:
    """
    Fetches the latest tags an image from docker hub
    """
    url = f"https://hub.docker.com/v2/repositories/{image}/tags?page_size=100"

    try:
        response = requests.get(url, timeout=10)
        response.raise_for_status()
        data = response.json()
        tags = [result["name"] for result in data.get("results", [])]
        return tags
    except requests.RequestException as e:
        print(f"Error fetching tags: {str(e)}", file=sys.stderr)
        sys.exit(1)


def latest_versions(tags: List[str], version_pattern=r"^(\d+\.\d+)-slim$") -> List[str]:
    """
    Get the 3 latest stable Python versions from Docker tags.
    Only considers slim-bullseye tags and excludes RCs and alpha/beta versions.

    Args:
        tags: List of Docker tag strings

    Returns:
        List of the 3 latest version tags
    """
    candidates = []
    for tag in tags:
        match = re.match(version_pattern, tag)
        if match:
            v = tag.split("-")[0]
            if any(x in v.lower() for x in ["rc", "alpha", "beta", "a", "b"]):
                continue

            candidates.append((v, tag))

    # sort major.minor versions and take top 3
    sorted_versions = sorted(
        candidates, key=lambda x: version.parse(x[0]), reverse=True
    )
    return [t[1] for t in sorted_versions[:3]]


def generate_pipeline(python_tags, node_versions) -> str:
    python_versions = [t.split("-")[0] for t in python_tags]
    py_json = json.dumps(python_versions)
    node_json = json.dumps(node_versions)
    latest_py = python_versions[0]
    latest_node = node_versions[0]
    return f"""---
stages:
  - build

.build-container:
  image: registry.gitlab.com/guardianproject-ops/docker-python-node:latest
  stage: build
  services:
    - docker:24.0.5-dind
  variables:
    DOCKER_NS: $CI_REGISTRY_IMAGE
    LATEST_PY: "{latest_py}"
    LATEST_NODE: "{latest_node}"
    DOCKER_TLS_CERTDIR: "/certs"
    DOCKER_CERT_PATH: "/certs/client"
    DOCKER_HOST: "tcp://docker:2376"
    DOCKER_TLS_VERIFY: 1
  script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
    - make -d init
    - make -d build-single DOCKER_NS=${{DOCKER_NS}} PY_VERSION=$PY_VERSION NODE_VERSION=$NODE_VERSION
    - make -d build-push DOCKER_NS=${{DOCKER_NS}} PY_VERSION=$PY_VERSION NODE_VERSION=$NODE_VERSION

build:
  extends: .build-container
  stage: build
  parallel:
    matrix:
      - PY_VERSION: {py_json}
        NODE_VERSION: {node_json}
  """


def generate_docs(python_tags, node_versions) -> str:
    python_versions = [t.split("-")[0] for t in python_tags]
    latest_py = python_versions[0]
    latest_node = node_versions[0]
    links = []
    for py_tag in python_versions:
        for node_ver in node_versions:
            if py_tag == latest_py and node_ver == latest_node:
                links.append(f"python{py_tag}-nodejs{node_ver} / latest")
            else:
                links.append(f"python{py_tag}-nodejs{node_ver}")
    links_str = "\n* ".join(links)
    docs = f"""
## Available Tags

[View Registry][gitlab_registry]

Image: registry.gitlab.com/guardianproject-ops/docker-python-node:TAG

* {links_str}

[gitlab_registry]: https://gitlab.com/guardianproject-ops/docker-python-node/container_registry/929627
"""
    return docs


def main():
    mode = None
    if len(sys.argv) != 2 or sys.argv[1] not in ["doc", "ci"]:
        print("ERROR: pass 'doc' or 'ci'")
        sys.exit(1)

    tags = fetch_image_tags("library/python")
    python_tags = latest_versions(tags)

    tags = fetch_image_tags("library/node")
    node_versions = latest_versions(tags, version_pattern=r"^(\d+)$")

    if sys.argv[1] == "doc":
        doc = generate_docs(python_tags, node_versions)
        print(doc)
        with open("docs/tags.md", "w") as f:
            f.write(doc)
    elif sys.argv[1] == "ci":
        yml = generate_pipeline(python_tags, node_versions)
        with open("generated.yml", "w") as f:
            f.write(yml)
        print("=======GENERATED CHILD PIPELINE YAML=======")
        print(yml)


if __name__ == "__main__":
    main()

export SHELL := $(shell which bash)
export README_DEPS ?=  docs/tags.md
.PHONY: docs/tags.md

-include $(shell curl -sSL -o .build-harness-ext "https://go.sr2.uk/build-harness"; echo .build-harness-ext)

export README_TEMPLATE_FILE := ${BUILD_HARNESS_EXTENSIONS_PATH}/templates/README_gp.md.gotmpl

DOCKER ?= docker
DOCKER_NS ?= guardianproject-ops/docker-python-node

build-single:
	echo "Building image ${DOCKER_NS}:python${PY_VERSION}-nodejs${NODE_VERSION}"
	${DOCKER} build --build-arg PY_IMAGE=docker.io/library/python:${PY_VERSION}-slim --build-arg NODE_VERSION=${NODE_VERSION} -t ${DOCKER_NS}:python${PY_VERSION}-nodejs${NODE_VERSION} ${PWD}
	${DOCKER} run ${DOCKER_NS}:python${PY_VERSION}-nodejs${NODE_VERSION} /test

build-push:
	${DOCKER} push ${DOCKER_NS}:python${PY_VERSION}-nodejs${NODE_VERSION}
	if [ "${PY_VERSION}" = "$$LATEST_PY" ] && [ "${NODE_VERSION}" = "$$LATEST_NODE" ]; then \
		docker tag ${DOCKER_NS}:python${PY_VERSION}-nodejs${NODE_VERSION} ${DOCKER_NS}:latest; \
		docker push ${DOCKER_NS}:latest; \
	fi


docs/tags.md:
	python3 versions.py doc

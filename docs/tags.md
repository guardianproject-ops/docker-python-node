
## Available Tags

[View Registry][gitlab_registry]

Image: registry.gitlab.com/guardianproject-ops/docker-python-node:TAG

* python3.13-nodejs23 / latest
* python3.13-nodejs22
* python3.13-nodejs20
* python3.12-nodejs23
* python3.12-nodejs22
* python3.12-nodejs20
* python3.11-nodejs23
* python3.11-nodejs22
* python3.11-nodejs20

[gitlab_registry]: https://gitlab.com/guardianproject-ops/docker-python-node/container_registry/929627
